package com.epam.course.renchka.task2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class App {

  public static void main(String[] args) {
    Droid[] droids = {
        new Droid("RR-28", 500, "bye-bye"),
        new Droid("RR-22", 400, "destroy"),
        new Droid("RM-30", 100, "oh no"),
    };
    Ship ship =
        new Ship(
            "Super2020", 1000, 500, droids);
    try (ObjectOutputStream out =
        new ObjectOutputStream(
            new FileOutputStream(
                "C:\\Projects\\epam\\task12-io-nio\\ship.out"))) {
      out.writeObject(ship);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    try (ObjectInputStream in =
        new ObjectInputStream(
            new FileInputStream(
                "C:\\Projects\\epam\\task12-io-nio\\ship.out"))) {
      Ship desShip = (Ship) in.readObject();
      System.out.println(desShip);
    } catch (IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
