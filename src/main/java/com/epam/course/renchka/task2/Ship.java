package com.epam.course.renchka.task2;

import java.io.Serializable;
import java.util.Arrays;

public class Ship implements Serializable {

  private String modelName;
  private int speed;
  private int passengersCapacity;
  private Droid[] droids;

  public Ship(String modelName, int speed, int passengersCapacity,
      Droid[] droids) {
    this.modelName = modelName;
    this.speed = speed;
    this.passengersCapacity = passengersCapacity;
    this.droids = droids;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  public int getSpeed() {
    return speed;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public int getPassengersCapacity() {
    return passengersCapacity;
  }

  public void setPassengersCapacity(int passengersCapacity) {
    this.passengersCapacity = passengersCapacity;
  }

  public Droid[] getDroids() {
    return droids;
  }

  public void setDroids(Droid[] droids) {
    this.droids = droids;
  }

  @Override
  public String toString() {
    return "Ship{" +
        "\n modelName='" + modelName + '\'' +
        "\n speed=" + speed +
        "\n passengersCapacity=" + passengersCapacity +
        "\n droids=" + Arrays.toString(droids) + "\n" +
        '}';
  }
}
