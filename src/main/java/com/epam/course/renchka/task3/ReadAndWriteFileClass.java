package com.epam.course.renchka.task3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

public class ReadAndWriteFileClass {

  private byte[] read(InputStream is) throws IOException {
    byte[] buffer = new byte[is.available()];
    int data;
    int i = 0;
    while ((data = is.read()) != -1) {
      buffer[i++] = (byte) data;
    }
    return buffer;
  }

  public void testUsualStreams() throws IOException {
    System.out.println("Test usual reader");
    try (
        InputStream is = new FileInputStream("D:/test/small.pdf");
        OutputStream os =
            new FileOutputStream("D:/test/small_copy.pdf")) {
      Date start = new Date();
      byte[] bytes = read(is);
      Date end = new Date();
      System.out.println("Usual reading time(ms): "
          + (end.getTime() - start.getTime()));

      System.out.println("Test usual writer");
      start = new Date();
      os.write(bytes);
      end = new Date();
      System.out.println("Usual writing time(ms): "
          + (end.getTime() - start.getTime()));
    }
  }

  public void testBufferedStreams() throws IOException {
    System.out.println("Test buffered reader");
    try (
        BufferedInputStream bis =
            new BufferedInputStream(
                new FileInputStream("D:/test/big.pdf"));
        BufferedOutputStream bos =
            new BufferedOutputStream(
                new FileOutputStream("D:/test/big_copy.pdf"))) {
      Date start = new Date();
      byte[] buffer = read(bis);
      Date end = new Date();
      System.out.println("buffered reading time(ms): "
          + (end.getTime() - start.getTime()));

      System.out.println("Test buffered writer");
      start = new Date();
      bos.write(buffer);
      end = new Date();
      System.out.println("buffered writing time(ms): "
          + (end.getTime() - start.getTime()));
    }
  }

  public static void main(String[] args) throws IOException {
    ReadAndWriteFileClass readAndWriteFile = new ReadAndWriteFileClass();
    readAndWriteFile.testUsualStreams();
    readAndWriteFile.testBufferedStreams();
  }
}
