package com.epam.course.renchka.task6;

import java.io.File;

public class Directory {

  public static void printContent(File file, String tab) {
    File[] files = file.listFiles();
    tab += "\t";
    for (File f : files) {
      if (f.isDirectory()) {
        System.out.printf("%s Directory: %s%n", tab, f.getName());
        printContent(f, tab);
      } else {
        System.out.printf("%s File: %s%n", tab, f.getName());
      }
    }
  }

  public static void main(String[] args) {
    printContent(new File("D:\\EPAM"), "\t");
  }
}
