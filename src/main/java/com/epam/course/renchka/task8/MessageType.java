package com.epam.course.renchka.task8;

public enum MessageType {
  USER_NAME,
  NAME_REQUEST,
  NAME_ACCEPTED,
  NAME_ALREADY_EXIST,
  TEXT,
  USER_ADDED,
  USER_REMOVED
}
