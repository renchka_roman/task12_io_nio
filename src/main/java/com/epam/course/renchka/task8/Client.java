package com.epam.course.renchka.task8;

import java.io.IOException;
import java.net.Socket;

public class Client {

  private static final String ADDRESS = "localhost";
  private static final int PORT = 8080;
  private Connection connection;
  private volatile boolean clientConnected;

  public class SocketThread extends Thread {

    private void printIncomingMessage(String message) {
      System.out.println(message);
    }

    private void informAboutAddingNewUser(String userName) {
      System.out.println(userName + " joined the chat.");
    }

    private void informAboutDeletingNewUser(String userName) {
      System.out.println(userName + " leave the chat.");
    }

    private void informAboutUserNameIsAlreadyExist(String userName) {
      System.out.printf(
          "User name \"%s\" is already exist. Try again.\n", userName);
    }

    private void notifyConnectionStatusChanged(boolean clientConnected) {
      Client.this.clientConnected = clientConnected;
      synchronized (Client.this) {
        Client.this.notify();
      }
    }

    private void clientHandshake()
        throws IOException, ClassNotFoundException {
      while (true) {
        Message message = connection.receive();
        if (message.getType() == MessageType.NAME_REQUEST) {
          connection.send(new Message(MessageType.USER_NAME, getUserName()));
        } else if (message.getType() == MessageType.NAME_ALREADY_EXIST) {
          informAboutUserNameIsAlreadyExist(message.getData());
        } else if (message.getType() == MessageType.NAME_ACCEPTED) {
          notifyConnectionStatusChanged(true);
          break;
        }
      }
    }

    private void clientMainLoop() throws IOException, ClassNotFoundException {
      while (true) {
        Message message = connection.receive();
        if (message.getType() == MessageType.TEXT) {
          printIncomingMessage(message.getData());

        } else if (message.getType() == MessageType.USER_ADDED) {
          informAboutAddingNewUser(message.getData());

        } else if (message.getType() == MessageType.USER_REMOVED) {
          informAboutDeletingNewUser(message.getData());
        }
      }
    }

    @Override
    public void run() {
      Socket socket = null;
      try {
        socket = new Socket(ADDRESS, PORT);
        Client.this.connection = new Connection(socket);
        clientHandshake();
        clientMainLoop();
      } catch (IOException | ClassNotFoundException e) {
        notifyConnectionStatusChanged(false);
      }
    }
  }

  public void run() {
    SocketThread socketThread = new SocketThread();
    socketThread.setDaemon(true);
    socketThread.start();
    synchronized (this) {
      try {
        wait();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    if (clientConnected) {
      System.out.println("Connected.\n"
          + "Input 'exit' to quit.");
      String text;
      while (!(text = ConsoleHelper.readString()).equals("exit")
          && clientConnected) {
        sendTextMessage(text);
      }
    } else {
      System.out.println("Happened some error.");
    }
  }

  private String getUserName() {
    System.out.println("Enter User name:");
    return ConsoleHelper.readString();
  }

  private void sendTextMessage(String text) {
    try {
      connection.send(new Message(MessageType.TEXT, text));
    } catch (IOException e) {
      clientConnected = false;
      System.out.println(e.getMessage());
    }
  }

  public static void main(String[] args) {
    Client client = new Client();
    client.run();
  }
}

