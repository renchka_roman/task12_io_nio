package com.epam.course.renchka.task8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {

  private static BufferedReader reader =
      new BufferedReader(new InputStreamReader(System.in));

  public static String readString() {
    boolean isReaded = false;
    String str = "";
    while (!isReaded) {
      try {
        str = reader.readLine();
        isReaded = true;
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    return str;
  }
}
