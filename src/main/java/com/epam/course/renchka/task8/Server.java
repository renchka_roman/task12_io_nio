package com.epam.course.renchka.task8;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {

  private static final int BACKLOG = 20;
  private static final String ADDRESS = "localhost";
  private static final int PORT = 8080;
  private static Map<String, Connection> connectionMap =
      new ConcurrentHashMap<>();

  private static class ClientHandler extends Thread {

    private Socket socket;

    public ClientHandler(Socket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
      System.out.println("Connected with " + socket.getRemoteSocketAddress());
      String name = null;
      try (Connection connection = new Connection(socket)) {
        name = registerUser(connection);
        notifyUsers(name);
        while (true) {
          Message message = connection.receive();
          if (message.getType() == MessageType.TEXT) {
            String str = name + ": " + message.getData();
            sendBroadcastMessage(name, new Message(MessageType.TEXT, str));
          }
        }
      } catch (ClassNotFoundException | IOException e) {
        System.out.println("Connection with " + name + " closed.");
      }
      if (name != null) {
        connectionMap.remove(name);
        sendBroadcastMessage(name, new Message(MessageType.USER_REMOVED, name));
      }
    }

    private String registerUser(Connection connection)
        throws IOException, ClassNotFoundException {
      Message message = null;
      String name = null;
      while (true) {
        connection.send(new Message(MessageType.NAME_REQUEST));
        if ((message = connection.receive()).getType()
            == MessageType.USER_NAME) {
          name = message.getData();
          if (name != null && !name.equals("")) {
            if (!connectionMap.containsKey(name)) {
              connectionMap.put(name, connection);
              connection.send(new Message(MessageType.NAME_ACCEPTED));
              break;
            } else {
              connection.send(
                  new Message(MessageType.NAME_ALREADY_EXIST, name));
            }
          }
        }
      }
      return name;
    }

    private void notifyUsers(String userName) throws IOException {
      for (Map.Entry<String, Connection> pair : connectionMap.entrySet()) {
        if (!pair.getKey().equals(userName)) {
          pair.getValue().send(new Message(MessageType.USER_ADDED, userName));
        }
      }
    }

    public static void sendBroadcastMessage(String userName, Message message) {
      for (Map.Entry<String, Connection> pair : connectionMap.entrySet()) {
        if (!pair.getKey().equals(userName)) {
          try {
            pair.getValue().send(message);
          } catch (IOException e) {
            System.out.println("Cannot sent a message to " + pair.getKey());
          }
        }
      }
    }
  }

  public static void main(String[] args) {
    try (ServerSocket serverSocket =
        new ServerSocket(PORT, BACKLOG, InetAddress.getByName(ADDRESS))) {
      System.out.printf("Listening on port %s.\n", serverSocket.getLocalPort());
      Socket socket;
      while (true) {
        socket = serverSocket.accept();
        new ClientHandler(socket).start();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
