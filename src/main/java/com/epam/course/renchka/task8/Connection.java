package com.epam.course.renchka.task8;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Connection implements AutoCloseable {

  private final Socket socket;
  private final ObjectOutputStream out;
  private final ObjectInputStream in;

  public Connection(Socket socket) throws IOException {
    this.socket = socket;
    out = new ObjectOutputStream(this.socket.getOutputStream());
    in = new ObjectInputStream(this.socket.getInputStream());
  }

  public void send(Message message) throws IOException {
    synchronized (out) {
      out.writeObject(message);
    }
  }

  public Message receive() throws IOException, ClassNotFoundException {
    synchronized (in) {
      return (Message) in.readObject();
    }
  }

  @Override
  public void close() throws IOException {
    in.close();
    out.close();
    socket.close();
  }
}

